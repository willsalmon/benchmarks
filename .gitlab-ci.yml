# This defines the CI pipeline for the BuildStream benchmarks project.
#
# Benchmarks need to be run on machines with consistent performance
# characteristics in order to be useful. Our normal CI infrastructure
# is based around virtual machines which are not predicatable enough
# to run benchmarks.
#
# We use the 'benchmarks' tag for runners which are reserved for
# running benchmarks.

cache:
  paths:
    - results_cache/

variables:
  BRANCH_REF: $CI_COMMIT_REF_NAME
  JOB_NAME_REF: $CI_JOB_NAME
  
stages:
  - clean
  - test-benchmarks
  - test-functional-test-simple
  - test-functional-test-combined-config
  - deploy

# Remove previous pipeline_caching
job:
  stage: clean
  script:
    - "rm -rf pipeline_cache/*"
  artifacts:
    paths: [ 'pipeline_cache' ]

benchmarks:
  stage: test-benchmarks
  tags: [ 'benchmarks' ]
  script:
    - if ! [ -z "${FOLLOW_BUILDSTREAM}" ]; then
    -   ./run_walk_ci.sh "-c bst_benchmarks/default.benchmark"
    - else
    -   ./ci_run.sh
    - fi
  dependencies:
    - job

  artifacts:
    paths: [ 'results_out/', 'results_cache/', 'pipeline_cache' ]

# Functional tests are only meant to check that the benchmarks code
# works correctly, not to measure anything. These are still tagged
# 'benchmarks' so they run on our runners, which have the necessary Python
# modules.
    
functional-test-simple:
  stage: test-functional-test-simple
  tags: [ 'benchmarks' ]
  script:
    - if ! [ -z "${FOLLOW_BUILDSTREAM}" ]; then
    -   ./run_walk_ci.sh "-c configs/simple.benchmark"
    - else
    -   ./ci_run.sh
    - fi
  artifacts:
    paths: [ 'results_out/', 'results_cache/', 'pipeline_cache' ]

functional-test-combined-config:
  stage: test-functional-test-combined-config
  tags: [ 'benchmarks' ]
  script:
    - if ! [ -z "${FOLLOW_BUILDSTREAM}" ]; then
    -   ./run_walk_ci.sh "-c configs/simple.benchmark -c configs/addon-version.benchmark"
    - else
    -   ./ci_run.sh
    - fi
  artifacts:
    paths: [ 'results_out/', 'results_cache/', 'pipeline_cache' ]

trigger_build:
  stage: deploy
  script:
    - if ! [ -z "${PUBLISH_RESULT}" ]; then
    -   "curl --request POST --form token=${IO_TOKEN} --form ref=master --form variables[JOB_REF]=$CI_JOB_URL https://gitlab.com/api/v4/projects/8992068/trigger/pipeline"
    - fi
  only:
    refs:
      - master
  artifacts:
    paths: [ 'results_out/', 'results_cache/', 'pipeline_cache/' ]
