#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Sam Thursfield <sam.thursfield@codethink.co.uk>
#        Jim MacArthur  <jim.macarthur@codethink.code.uk>
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

# buildstream benchmark script

import click
import docker
import requests

import datetime
import io
import json
import logging
import os
import shutil
import sys
import tarfile
import tempfile
import git

from . import config
from . import hostanalysis


DEFAULT_CONFIG_FILE = os.path.join(os.path.dirname(__file__), 'default.benchmark')

# We use Docker image labels to identify the contents of our prepared images,
# enabling reuse. The documentation says "Authors of third-party tools should
# prefix each label key with the reverse DNS notation of a domain they own, such
# as com.example.some-label.".
#
# See: https://docs.docker.com/config/labels-custom-metadata/
DOCKER_LABEL_NAMESPACE = 'org.gnome.buildstream.benchmarks'

# This is how we name the Docker images we create.
DOCKER_REPOSITORY = 'bst_benchmarks'


class BstVersion():
    """Represents a version of BuildStream being tested.

    Args:
        spec (config.BstVersionSpec): Version specification
        image (docker.models.images.Image): Image to be used for running tests for this version
        base_docker_digest (str): Exact sha256 digest of the base Docker image (from which 'image' was built).
        buildstream_commit (str): Exact sha1 hash of the BuildStream commit.
        bst_version_string (str): Output of `bst --version` inside the container.
        buildstream_commit_date (str): Date/time of BuildStream commit.
    """

    def __init__(self, spec, image, base_docker_digest, buildstream_commit, bst_version_string, buildstream_commit_date):
        self.spec = spec
        self.image = image
        self.base_docker_digest = base_docker_digest
        self.buildstream_commit = buildstream_commit
        self.bst_version_string = bst_version_string
        self.buildstream_commit_date = buildstream_commit_date

    def describe(self):
        return {
            'name': self.spec.name,
            'base_docker_image': self.spec.base_docker_image,
            'base_docker_ref': self.spec.base_docker_ref,
            'base_docker_digest': self.base_docker_digest,
            'buildstream_repo': self.spec.buildstream_repo,
            'buildstream_ref': self.spec.buildstream_ref,
            'buildstream_commit': self.buildstream_commit,
            'buildstream_commit_date': self.buildstream_commit_date,
            'bst_version_string': self.bst_version_string,
        }


class TestResult():
    """Results of running a specific test against some versions of BuildStream.

    Args:
        spec (config.TestSpec): Test specification
    """
    def __init__(self, spec):
        self.spec = spec

        self.results_for_version = dict()

    def record_result_for_version(self, version, average_measurements, returncode, output, exception):
        """Record results for this test against a specific version of BuildStream.

        Args:
            version (BstVersion): Version of BuildStream that was tested.
            average_measurements (dict): Measurements returned by the test script, or None
            returncode (int): Exit code of the test script, or None if it didn't run
            output (str): Output from the test script, or None if it didn't run
            exception (Exception): Exception if something broke internally, or None
        """
        result = {
            'version': version.spec.name,
        }

        if exception:
            result['exception'] = str(exception)

            # For successful runs we don't share the output to avoid noise, but
            # if anything failed it's useful to see what.
            if output:
                result['output'] = str(output)

        if returncode:
            result['returncode'] = returncode

        if average_measurements:
            result['measurements'] = average_measurements
            result['repeats'] = self.spec.repeats

        self.results_for_version[version.spec.name] = result

    def describe(self):
        return {
            'name': self.spec.name,
            'results': list(self.results_for_version.values())
        }


def get_image_label(image, label_name):
    return image.labels['{}.{}'.format(DOCKER_LABEL_NAMESPACE, label_name)]


def compare_image_label(image, label_name, expected_value):
    value = get_image_label(image, label_name)
    if value == expected_value:
        logging.debug("Label {} matches expected value {}".format(label_name, expected_value))
        return True
    else:
        logging.debug("Label {} has value {}, wanted value {}".format(label_name, value, expected_value))
        return False


# Looks for an existing Docker image corresponding to 'version_spec'.
#
# The image metadata is checked to ensure that it matches the specified
# version. However we do not check for updates to the base Docker image or the
# commit of buildstream.git.
def reuse_prepared_version(docker_client, version_spec):
    image_name = '{}:{}'.format(DOCKER_REPOSITORY, version_spec.name)

    try:
        image = docker_client.images.get(image_name)
        logging.info("Found existing image named {}.".format(image_name))

        if all([compare_image_label(image, 'base-docker-image', version_spec.base_docker_image),
                compare_image_label(image, 'base-docker-ref', version_spec.base_docker_ref),
                compare_image_label(image, 'buildstream-repo', version_spec.buildstream_repo),
                compare_image_label(image, 'buildstream-ref', version_spec.buildstream_ref)]):
            base_docker_digest = get_image_label(image, 'base-docker-digest')
            buildstream_commit = get_image_label(image, 'buildstream-commit')
            bst_version_string = get_image_label(image, 'bst-version-string')
            buildstream_commit_date = version_spec.buildstream_commit_date

            return BstVersion(version_spec, image, base_docker_digest, buildstream_commit, bst_version_string, buildstream_commit_date)
        else:
            logging.info("Image does not match the version spec, not reusing the image.")
            return None
    except docker.errors.ImageNotFound:
        logging.info("Did not find existing image named {}".format(image_name))
        return None


def parse_output_var(line, prefix):
    if line.startswith(prefix + ': '):
        return line.split(': ', 1)[1]
    else:
        raise RuntimeError("Line didn't start with expected prefix {}: {}".format(prefix, line))


# Creates a Docker image that can be used to test a specific version of
# BuildStream, as described by 'version_spec'.
#
# If 'reuse_images' is set to True, existing images with the same name will be
# considered for reuse. Docker labels are used to check whether an image matches
# the given spec.
def prepare_version(docker_client, version_spec, reuse_images=False):
    logging.info("Preparing image '{}:{}'".format(DOCKER_REPOSITORY, version_spec.name))

    # The base image may have a symbolic name, so check that we have the latest
    # version.
    logging.debug("Pulling base Docker image {}:{}".format(version_spec.base_docker_image, version_spec.base_docker_ref))
    docker_client.images.pull(version_spec.base_docker_image, version_spec.base_docker_ref)

    base_docker_image_with_version = '{}:{}'.format(version_spec.base_docker_image, version_spec.base_docker_ref)

    # The digest identifies the *exact* Docker image that we are using as a base.
    base_docker_image = docker_client.images.get(base_docker_image_with_version)
    base_docker_digest = base_docker_image.id

    # Start a container from the base image and run the setup script.
    #
    # Due to Docker limitations, we run setup as a single shell script. Luckily
    # we are only interpolating URLs and Git refs into this which should not
    # contain spaces or other weird characters.
    commands = [
        'set -e',
        'git clone {} ./buildstream --branch {}'.format(version_spec.buildstream_repo, version_spec.buildstream_ref)
    ]

    if version_spec.buildstream_commit:
        commands += ['cd buildstream',
        'git checkout {}'.format(version_spec.buildstream_commit),
        'cd ..',
    ]

    commands +=  ['pip3 install --user ./buildstream',
        'echo "buildstream_commit: $(git -C ./buildstream rev-parse HEAD)"',
        'echo "bst_version_string: $(bst --version)"']

    script = '\n'.join(commands)

    container = docker_client.containers.run(base_docker_image_with_version,
                                             command=['/bin/bash', '-c', script],
                                             detach=True)

    try:
        response = container.wait()
        exit_code = response['StatusCode']
        if exit_code != 0:
            error = container.logs(stdout=True, stderr=True).decode('unicode-escape')
            raise RuntimeError("Container setup script exited with code {}. Error output:\n{}"
                               .format(exit_code, error))

        output = container.logs(stdout=True).decode('unicode-escape')
        logging.debug("Output from setup script: {}".format(output))

        lines = output.splitlines()

        buildstream_commit = parse_output_var(lines[-2], 'buildstream_commit')
        bst_version_string = parse_output_var(lines[-1], 'bst_version_string')
        buildstream_commit_date = version_spec.buildstream_commit_date

        label_changes = '\n'.join([
            'LABEL {}.base-docker-image="{}"'.format(DOCKER_LABEL_NAMESPACE, version_spec.base_docker_image),
            'LABEL {}.base-docker-ref="{}"'.format(DOCKER_LABEL_NAMESPACE, version_spec.base_docker_ref),
            'LABEL {}.base-docker-digest="{}"'.format(DOCKER_LABEL_NAMESPACE, base_docker_digest),
            'LABEL {}.buildstream-repo="{}"'.format(DOCKER_LABEL_NAMESPACE, version_spec.buildstream_repo),
            'LABEL {}.buildstream-ref="{}"'.format(DOCKER_LABEL_NAMESPACE, version_spec.buildstream_ref),
            'LABEL {}.buildstream-commit="{}"'.format(DOCKER_LABEL_NAMESPACE, buildstream_commit),
            'LABEL {}.bst-version-string="{}"'.format(DOCKER_LABEL_NAMESPACE, bst_version_string),
        ])

        image = container.commit(repository=DOCKER_REPOSITORY, tag=version_spec.name, changes=label_changes)
    except:
        container.remove()
        raise

    return BstVersion(version_spec, image, base_docker_digest,
                      buildstream_commit, bst_version_string, buildstream_commit_date)


# Prepares a Docker volume that can be mounted during test runs.
def prepare_volume(docker_client, volume_spec, versions_dict):
    logging.info("Preparing volume '{}'".format(volume_spec.name))

    try:
        version = versions_dict[volume_spec.prepare.version]
    except KeyError:
        raise RuntimeError("Volume requires unknown version: '{}'".format(volume_spec.prepare.version))

    volume = docker_client.volumes.create(name=volume_spec.name)

    container = docker_client.containers.run(
        version.image, command=['/bin/bash', '-c', volume_spec.prepare.script],
        detach=True,
        mounts=[
            docker.types.Mount(target=volume_spec.prepare.path, source=volume.id)
        ])

    try:
        response = container.wait()
        exit_code = response['StatusCode']
        if exit_code != 0:
            error = container.logs(stdout=True, stderr=True).decode('unicode-escape')
            raise RuntimeError("Volume setup script exited with code {}. Error output:\n{}"
                               .format(exit_code, error))

        output = container.logs(stdout=True).decode('unicode-escape')
        logging.debug("Output from setup script: {}".format(output))
    finally:
        container.remove()

    return volume


# Read the measurements file from inside the container that has run a test.
def read_measurements(container, path):
    logging.debug("Reading measurements file from path: {}".format(path))

    with tempfile.NamedTemporaryFile('wb') as tar:
        stream, stat = container.get_archive(path)

        # This is an iterator since docker-py 3.0.0, so we can't stream
        # it directly to tarfile.open() without writing a custom adapter.
        for chunk in stream:
            tar.write(chunk)
        tar.flush()

        # The good thing is we can stream the file we want back out; this
        # wouldn't be possible if we passed a stream to tarfile.open() because
        # the extractfile() method needs to be able to seek backwards.
        with tarfile.open(tar.name, mode='r') as tar:
            data = tar.extractfile(os.path.basename(path))
            text = io.TextIOWrapper(data, encoding='utf-8')
            measurements = json.load(text)

    return measurements


# Runs one test against one version of BuildStream, with one or more repeats.
#
# The return value is a tuple of:
#
#   (measurements, returncode, error output, exception)
#
# If the test returns a non-zero exit code or causes an exception then this
# function will return immediately, even if it was meant to repeat the test more
# times.
def run_benchmark(docker_client, version, test_spec, volumes_dict):
    logging.info("Running test '{}' on version '{}'. Repeats: {}"
                 .format(test_spec.name, version.spec.name, test_spec.repeats))

    all_measurements = []

    for i in range(0, test_spec.repeats):
        logging.debug("Starting container from {}".format(version.image))

        mounts = []
        for mount in test_spec.mounts:
            volume = volumes_dict[mount.volume]
            mounts.append(
                docker.types.Mount(target=mount.path, source=volume.id)
            )

        container = docker_client.containers.run(
            version.image,
            command=['/bin/bash', '-c', test_spec.script],
            detach=True,
            devices='/dev/fuse:/dev/fuse:rwm',
            labels=['buildstream-benchmark'],
            mounts=mounts,
            privileged=True,
        )

        try:
            logging.debug("Waiting for container to finish")

            response = container.wait()
            returncode = response['StatusCode']
            output = container.logs().decode('unicode-escape')

            logging.debug("Output: {}".format(output))
            logging.debug("Returncode: {}".format(returncode))

            if returncode != 0:
                raise RuntimeError("Test script returned non-zero exit code.")

            measurements = read_measurements(container, test_spec.measurements_file)

            all_measurements.append(measurements)

        except Exception as e:
            logging.error("Error during test (iteration {}): {}".format(i, e))
            exception = e

            return all_measurements, returncode, output, exception
        finally:
            container.stop()
            container.remove()

    logging.info("Test '{}' completed on version '{}'".format(test_spec.name, version.spec.name))

    return all_measurements, 0, "", None

@click.command()
@click.option('config_files', '--config-file', '-c', type=click.Path(),
              help="YAML description of tests to run, overriding the defaults.",
              multiple=True)
@click.option('--debug/--no-debug', default=False)
@click.option('--keep-images/--no-keep-images', default=False,
              help="Do not delete the Docker images that we create.")
@click.option('--reuse-images/--no-reuse-images', default=False,
              help="Reuse test images from previous runs if found. Beware that "
                   "changes in remote refs will not be picked up if images are "
                   "reused. (Implies --keep-images)")
@click.option('output_file', '--output-file', '-o', type=click.Path(),
              help="Output file definition", default=None)
def run(config_files, debug, keep_images, reuse_images, output_file):
    if debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    else:
        logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    start_time = datetime.datetime.now()
    logging.info("BuildStream benchmark runner started at {}".format(start_time))

    benchmark_config = config.BenchmarkConfig()

    # Only load the default config if no config was specified. If one or more was
    # specified, load them all in order.
    if len(config_files) == 0:
        benchmark_config = config.load(DEFAULT_CONFIG_FILE, benchmark_config)
    else:
        for config_filename in config_files:
            benchmark_config = config.load(config_filename, benchmark_config)

    if len(benchmark_config.test_specs) == 0:
        raise RuntimeError("You must have at least one test specified in your "
                           "config file - none were found.")

    if len(benchmark_config.version_specs) == 0:
        raise RuntimeError("You must have at least one Buildstream version specified "
                           "in your config file - none were found.")

    host_info = hostanalysis.get_host_info()

    docker_client = docker.from_env()

    benchmarking_git_sha = git.Repo(search_parent_directories=True).head.object.hexsha

    try:
        docker_client.ping()
    except requests.exceptions.ConnectionError as e:
        raise RuntimeError("Could not connect to Docker daemon: socket not found. "
                           "Set DOCKER_HOST in the environment if it is at a "
                           "non-standard path.".format(e))
    except Exception as e:
        raise RuntimeError("Could not connect to Docker daemon: {}".format(e))

    versions_dict = {}
    for version_spec in benchmark_config.version_specs:
        version = None
        if reuse_images:
            version = reuse_prepared_version(docker_client, version_spec)
        if version is None:
            version = prepare_version(docker_client, version_spec)
        versions_dict[version_spec.name] = version

    volumes_dict = {}
    for volume_spec in benchmark_config.volume_specs:
        volume = prepare_volume(docker_client, volume_spec, versions_dict)
        volumes_dict[volume_spec.name] = volume

    results_list = []
    for test in benchmark_config.test_specs:
        result = TestResult(test)
        for version in versions_dict.values():
            result_for_version = run_benchmark(docker_client, version, test, volumes_dict)
            result.record_result_for_version(version, *result_for_version)
        results_list.append(result)

    if not keep_images and not reuse_images:
        logging.info("Deleting test-specific Docker images")
        for version in versions_dict.values():
            docker_client.images.remove(version.image.id)

    # for volume in volumes:
    #   volume.remove()
        

    end_time = datetime.datetime.now()
    logging.info("BuildStream benchmark runner finished at {}".format(end_time))

    logging.info("Writing results to stdout")
    output = {
        'start_timestamp': start_time.timestamp(),
        'end_timestamp': end_time.timestamp(),
        'host_info': host_info,
        'benchmarking_sha': benchmarking_git_sha,
        'versions': [ version.describe() for version in versions_dict.values()],
        'tests': [ result.describe() for result in results_list]
    }
    if output_file:
        logging.info("Writing to file {}".format(output_file))
        try:
            with open(output_file, 'w') as fp:
                json.dump(output, fp, indent=4)
        except IOError as err:
            logging.error("Unable to write results to file: {}".format(output_file))
        except TypeError as er:
            logging.error("Unable to generate json file")
    else:
        logging.info("Writing results to stdout")
        json.dump(output, sys.stdout, indent=4)


try:
    run(prog_name="benchmark")
except RuntimeError as e:
    sys.stderr.write("{}\n".format(e))
    sys.exit(1)
