#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import yaml
import os
import logging
import datetime
import git

from dateutil.parser import parse

# A run token encapsulates data that describes the dynamics of the last
# benchmarking testing that was carried out and retained. This data is
# used to determine which SHAs need to be processed (i.e. the buildstream
# commits that were made since last time) and confirm consistency between
# buildstream and benchmarking branches/commits. A check is also made as
# to whether the expected last result that was written is still present
# and has not been superseeded by a failed run (warning at the minute).


# Function to parse a run token file and return the contents.

def process_token_file(file_path):
   try:
      with open(file_path, "r") as config:
         try:
            data_loaded = yaml.load(config)
            time = data_loaded['build']['time']
            buildstream_sha = data_loaded['build']['bs_sha']
            buildstream_branch = data_loaded['build']['bs_branch']
            benchmark_sha = data_loaded['build']['bm_sha']
            benchmark_branch = data_loaded['build']['bm_branch']
            last_result = data_loaded['build']['file']
         except yaml.YAMLError as y_err:
            logging.error("Unable to parse process token file: ", y_err)
            raise
   except OSError as err:
      logging.error("Unable to access process token file: ", err)
      raise err

   return {'build': {'time': time, 'bs_sha': buildstream_sha, 'bs_branch': buildstream_branch,
           'bm_sha': benchmark_sha,'bm_branch': benchmark_branch, 'result': last_result}}


# Verify the token data against a given configuration (i.e. the configuration for the
# current run - return false if there is an inconsistency with the possibility of having
# to regenerate a tenable configuration.

def verify_token_data( token_data, last_result, results_path, repo_path ):

   # Check time parses
   try:
      parse_time = parse(token_data['build']['time'])
   except ValueError as v_error:
      logging.error("Time from token file does not resolve: ", v_error)
      return False

   # Check time makes sense
   now = datetime.datetime.now()
   if now < parse_time:
      logging.error("Time from token is later than current resolved time: ", now, parse_time)
      return False

   # Check buildstream_sha is still valid
   # assumes repo has already been cloned elsewhere
   try:
      if os.path.exists(repo_path):
         # Init repo from repo path
         repo = git.Repo.init(repo_path, bare=False)
         # Check buildstream branch is valid
         if not repo.git.rev_parse('--verify', token_data['build']['bs_branch']):
            logging.error('Branch not in repo: ', token_data['build']['bs_branch'])
            return False
         # Check sha is still valid
         if token_data['build']['bs_sha'] not in [commit.hexsha for commit in repo.iter_commits(token_data['build']['bs_branch'])]:
            logging.error('SHA of last commit not present in current repo branch')
            return False
      else:
         logging.error('Git Repo not valid')
         return False
   except git.exc.GitError as err:
      logging.error("Unable to access git repository: ", err)
      return False
   except Exception as g_ex:
      logging.error("Exception when attempting to validate buildstream details: ", g_ex)
      return False

   # Check if previous bench mark sha is valid in the context of the current repo
   try:
      # Init repo based upon parent directory
      bench_mark_repo = git.Repo(search_parent_directories=True)
      # Check benchmark branch is valid
      if not bench_mark_repo.git.rev_parse('--verify', token_data['build']['bm_branch']):
         logging.error('Branch not in repo: ', token_data['build']['bs_branch'])
         return False
      if token_data['build']['bm_sha'] not in [commit.hexsha for commit in bench_mark_repo.iter_commits(token_data['build']['bm_branch'])]:
         logging.error('SHA of previous benchmarking Head not in repo branch')
         return False
   except git.exc.GitError as err:
      logging.error("Unable to verify benchmark sha: ", err)
      return False

   # Check if the last results file exists - this could become a check of the larger set
   # of results files.
   if last_result:
      last_result = os.path.join(results_path, token_data['build']['result'])
      if not os.path.exists(last_result):
         logging.error('The last results file from the previous run does not exist')
         return False

   return True

# Generate a token file given certain parameters, used if no token file is present
# or the token file that is currently in place needs replacing.

def generate_token_file(file_path, buildstream_sha, buildstream_branch, last_result):

   # Init repo based upon parent directory
   bench_mark_repo = git.Repo(search_parent_directories=True)

   # Write configuration file for token file
   data = dict( 
      build = dict(
      time = str(datetime.datetime.now()),
      bs_sha = str(buildstream_sha),
      bs_branch = str(buildstream_branch),
      bm_sha = str(bench_mark_repo.head.object.hexsha),
      bm_branch = str(bench_mark_repo.head.name),
      file = str(last_result))
   )
   try:
      with open(file_path, 'w') as outfile:
         yaml.dump(data, outfile, default_flow_style=False)
   except:
       logging.error("Unable to write configuration file: ", sys.exc_info()[0])
       raise
