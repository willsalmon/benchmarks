#!/bin/bash

BENCHMARK_CONF=""
if [ $# -ne 0 ]; then
  BENCHMARK_CONF="$1"
fi

eval "export BUILDSTREAM_HEAD=$(git ls-remote https://gitlab.com/BuildStream/buildstream.git HEAD | awk '{ print $1 }')"
echo $BUILDSTREAM_HEAD
python3 generate_buildstream_config.py -o bs_catchup.benchmark -r 'results_cache/' -t 'results_cache/run_token.yml'
cp 'bs_catchup.benchmark' 'results_cache/'
BENCHMARK_CONF="$BENCHMARK_CONF -c bs_catchup.benchmark"
./ci_run.sh "$BENCHMARK_CONF"
python3 update_run_token.py -t 'results_cache/run_token.yml' -i 'results_cache/'
