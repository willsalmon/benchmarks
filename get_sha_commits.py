#!/usr/bin/python3

#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import git
import tempfile
import argparse
import shutil
import re
import os
import logging
import sys

# This function generates a list of SHAs for a given repo, branch a start commit
# which is the last commit that was processed and is prior to the 1st commit to
# be returned and the last commit which might be current head as determined at
# the beginning of benchmark testing.
#
# repo_url - the url of the repo that should be considered
# branch - the buildstream branch that should be considered
# last_commit - the last commit that was previously processed
# latest_commit - the last commit that needs to be added to the returned
#                 list.

def main():
   repo = "https://gitlab.com/BuildStream/buildstream.git"
   branch = 'master'
   last_commit = None
   latest_commit = None

   parser = argparse.ArgumentParser()
   parser.add_argument("-r", "--repo_url",
                       help="URL of the repo",
                       type=str)
   parser.add_argument("-b", "--branch",
                       help="Branch to be traversed",
                       type=str)
   parser.add_argument("-s", "--last_commit",
                       help="Last commit SHA1 reference",
                       type=str)
   parser.add_argument("-t", "--latest_commit",
                       help="Latest commit SHA1 reference",
                       type=str)
   args = parser.parse_args()

   if bool(args.repo_url):
      repo = args.repo_url

   if bool(args.branch):
      branch = args.branch

   if bool(args.last_commit):
      last_commit = args.last_commit

   if bool(args.latest_commit):
      latest_commit = args.latest_commit

   try:
      commits = get_list_of_commits(repo, branch, last_commit, latest_commit)
   except git.exc.GitError as err:
      print("Unable to extract commits: ", err)
      sys.exit(1)
   except Exception as ex_err:
      print("Nothing to extract: ", ex_err)
      sys.exit(1)

   print(commits)


def get_list_of_commits(repo_path, branch, lastCommit, latestCommit):

   commits = list()

   with tempfile.TemporaryDirectory(prefix='temp_staging_location') as temp_staging_area:
      try:
         if os.path.exists(repo_path):
            repo = git.Repo.init(repo_path, bare=False)
         else:
            repo = git.Repo.clone_from(repo_path, temp_staging_area)
      except git.exc.GitError as err:
         logging.error("Unable to access git repository: ", err)
         raise

      start = False
      for commit in repo.iter_commits(branch):
         if commit.hexsha == latestCommit:
             commits.append(commit)
             start = True
             if commit.hexsha == lastCommit:
                break
         elif commit.hexsha == lastCommit:
             break
         elif start == True:
             commits.append(commit)

   return commits


if __name__ == "__main__":
   main()
