#!/usr/bin/python3

import token_file_processing
import os
import logging
import shutil
import tempfile
import sys

# Taken from https://stackoverflow.com/questions/19425736/how-to-redirect-stdout-and-stderr-to-logger-in-python
class LoggerWriter:
    def __init__(self, level):
        # self.level is really like using log.debug(message)
        # at least in my case
        self.level = level

    def write(self, message):
        # if statement reduces the amount of newlines that are
        # printed to the logger
        if message != '\n':
            self.level(message)

    def flush(self):
        # create a flush method so things can be flushed when
        # the system wants to. Not sure if simply 'printing'
        # sys.stderr is the correct way to do it, but it seemed
        # to work properly for me.
        self.level(sys.stderr)


def main():
   # Get stderr reporting to logging
   log = logging.getLogger()
   sys.stdout = LoggerWriter(log.debug)
   sys.stderr = LoggerWriter(log.warning)
  
   try:
      # Create temporary staging area
      temp_staging_area = tempfile.mkdtemp(prefix='temp_staging_location')
      # Get logging reporting to file that can be interogated
      log_filename = os.path.join(temp_staging_area, 'error_file.txt')
      logging.basicConfig(filename=log_filename)
      # Clone buildstream repo to temporary staging area
      repo = git.Repo.clone_from('https://gitlab.com/BuildStream/buildstream.git', temp_staging_area)
      # Get buildstream head SHA
      test_sha = str(repo.head.object.hexsha)
      # Cloned buildstream repo path
      buildstream_path = os.path.join(temp_staging_area, 'buildstream')

      # Check that reference token file parses and is valid
      token_data = token_file_processing.process_token_file('bst_benchmarks/test_run_token.yml')
      valid = token_file_processing.verify_token_data(token_data, '', '', buildstream_path)

      assert valid

      # Check that a token file can be generated, that this can then be parsed and verified
      token_file_processing.generate_token_file('test.yml', test_sha, 'master', '')
      token_data = token_file_processing.process_token_file('test.yml')
      valid = token_file_processing.verify_token_data(token_data, '', '', buildstream_path)

      assert valid

      # Check that an invalid token file can be generated and that this fails verification
      # and failure is reported through stderr logging to file
      token_file_processing.generate_token_file('test.yml', test_sha, 'faux-master', '')
      token_data = token_file_processing.process_token_file('test.yml')
      valid = token_file_processing.verify_token_data(token_data, '', '', buildstream_path)

      assert not valid
      assert 'fatal: Needed a single revision' in open(log_filename)
   finally:
      shutil.rmtree(temp_staging_area)

if __name__ == "__main__":
   main()
