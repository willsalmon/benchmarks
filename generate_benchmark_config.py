#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import os
import logging
import shutil
import tempfile

from distutils.file_util import copy_file

# This function generates a benchmark configuration file that allows for
# multiple buildstream commits to be benchmarked individually.
#
# output_file - the full path for the generated benchmark configuration file
# template_file - the full path to the templat file that is to be used to
#                 generate the benchmark configuration file.
# list_of_shas - list of Buildstream commits that need to be processed
# docker_version - the docker version to be used in the configuration, set
#                  to latest, but might be more restricted in the future
# bs_branch - the Buildstream branch that is being considered, defaults to
#             master

def generate_benchmark_configuration(output_file="generated.benchmark", template_file="bst_benchmarks/template.benchmark", list_of_shas=[], docker_version="latest", bs_branch='master'):

   # Create temporary staging area for generated benchmark configuration
   temp_staging_area = tempfile.mkdtemp(prefix='temp_staging_location')
   temp_benchmark = os.path.join(temp_staging_area, 'temp_benchmark.benchmark')

   try:
      # Check template file exists and then copy into temporary generated benchmark
      # configuration file for processing
      if os.path.isfile(template_file):
         shutil.copyfile(template_file, temp_benchmark)
         logging.info("Template file is: ", template_file)
      else:
         logging.error("Specified template file does not exist: ", template_file)
         raise Exception("Specified template file does not exist: ", template_file)

      # Open the temporary benchmark configuration file and read
      with open(temp_benchmark) as f:
         page = f.readlines()

      # New benchmarking data
      new_file = ""
      # Header for versions
      generic_version_entry = ''
      # Go through each line of the template and copy everything apart from the
      # stubbed generic version entry to the new benchmarking data. The stubbed
      # entries are copied into a generic version entry for later use.
      for line in page:
         if '<name_stub>' in line:
            generic_version_entry += line
         elif '<docker_stub>' in line:
            generic_version_entry += line
         elif '<buildstream_ref_stub>' in line:
            generic_version_entry += line
         elif '<buildstream_stub>' in line:
            generic_version_entry += line
         elif '<buildstream_time_stub>' in line:
            generic_version_entry += line
         else:
            new_file += line
      generic_version_entry += '\n\n'

      # Iterate through the list of shas and populate the stubbed entries
      # with sha data from the entry, then write each entry to the new file
      # entry
      for entry in list_of_shas:
         new_file += generic_version_entry.replace('<name_stub>', str(entry)) \
                                          .replace('<docker_stub>', docker_version) \
                                          .replace('<buildstream_stub>', str(entry)) \
                                          .replace('<buildstream_ref_stub>', bs_branch) \
                                          .replace('<buildstream_time_stub>', str(entry.committed_date))

      # Write the new file entry back to the temporary file
      with open(temp_benchmark, 'w') as f:
         f.write(new_file)

      # Copy the temporary benchmark file to the requested destination
      try:
         copy_file(temp_benchmark, output_file)
      except OSError as err:
         logging.error("Unable to copy pages to target: ", args.output_path)
         raise Exception('Unable to create target configuration file: ', err)

   finally:
      shutil.rmtree(temp_staging_area)
